Modlet Installation
===================

These instructions will tell you how to add Castle Spartan to the other game POI and have it spawn in your randomly created world.

Download the modlet
-------------------

You need [CastleSpartan_3.0.zip](https://gitlab.com/spartan-designs/castle-spartan/raw/master/Releases/CastleSpartan_3.0.zip). This file includes both POI and modlet files in the _Mods_ folder.

The instructions are slightly different if you play _7 Days to Die_ on a stand-alone PC or if you use a dedicated server.

For stand-alone PC installation
-------------------------------

Copy the **CastleSpartan** folder into your 7 Days do Die `Mods` folder.

Optional: copy the `Mods\CastleSpartan\Prefabs` files into your game `Data/Prefabs`. This will allow you to see the castle using the in game POI editor.


For dedicated server installation
---------------------------------

It's pretty much the same thing then the stand-alone PC installation, you copy the **CastleSpartan** folder into the server `Mods` folderon your server. Most hosting services provide FTP access that will allow you to upload the files to the right folder.

The players do not need to install the mod, it only has to be installed on the server.

* [More information on how to install modlets](https://7daystodie.gamepedia.com/How_to_Install_Modlets)

How to use the mod
==================

Once the mod is installed, randomly generated worlds will have a chance to spawn Castle Spartan. You just have to generate a new world and the castle should be in there somewhere. There should be multiple instances of the castle in your new world.

As you may guess, this will only work with newly generated worlds. Any world that was generated before you installed the mod will not spawn the castle. You will also need to start a new game in order to use that new world.

How to see if the Castle actually spawned in my generated world
===============================================================

Once the world is generated, you will find a folder with the world name near where the save games are located. On my PC, the folder for the generated worlds is `%APPDATA%\7DaysToDie\GeneratedWorlds`. (I'm using a Windows 10 box.) For dedicated servers, you might find the folder by looking at the **UserDataFolder** in the **serverconfig.xml** file. You will need to find where that folder is on your PC or Server. 

Once you found the folder named for your world (something like _South Pidimo County_ or _West Wikeno Territory_), you will find a file named `prefabs.xml` inside. If you open that file with any text editor like Notepad, you can search for `CastleSpartan_v3`. If you find it, you know that your random world has spawned the POI.
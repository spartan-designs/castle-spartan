# Castle Spartan

**Castle created by Grand Spartan for 7 Days to Die Alpha 17 and 18**

## Description

![Castle Spartan](https://gitlab.com/spartan-designs/castle-spartan/-/raw/master/Mods/CastleSpartan/Prefabs/CastleSpartan_v3.jpg)

Castle Spartan is a base design for the game [7 Days to Die](https://store.steampowered.com/app/251570/7_Days_to_Die/) that that was mostly built during [Grand Spartan live streaming](https://www.youtube.com/playlist?list=PL7iVZ8VsXQwl354rybNJO91H9J0_m1XUS). You can watch the [Castle video preview here](https://youtu.be/0Wd9GcGGyvc).

The one thing missing for me was the ability to do a virtual visit of the Castle. Fortunately, Grand Spartan was kind enough to give me access to the save game files and I was able to extract the Castle and create a 7 Days to Die POI (Point of Interest). This project's goal is to allow players to download the POI to their computer and interact with it in using the game.

Have a look at [Grand Spartan](https://www.youtube.com/user/MrMMAFAN76) Youtube channel. There is a lot of very fun and interesting stuff there. If you like the POI or any of the videos, be sure to subscribe and like. Grand Spartan will really appreciate it.

If you interested by other mods made by Éric Beaudoin, [click here](https://github.com/Laotseu/7dtdMods/blob/master/README.md)

## How to use the POI

### Overview

This is an overview of the steps needed to do to install and use the POI files. Details instructionS for each step follow.

1. Install 7 Days to Die on your computer. The POI v1 and v2 have been tested with **Alpha 17.4 (b4)**. v3 has been tested with **Alpha 18.4 (b4)**
2. [Download](https://gitlab.com/spartan-designs/castle-spartan/-/releases) the POI files and copy them into the game **Prefabs folder**.
3. Launch the game Prefab editor and load the **CastleSpartan** POI.
4. Start the **Playtest** mode and enjoy your visit.

#### Wait, I want Castle Spartan in my game, not just for a playtest!

If you would rather have Castle Spartan spawn in your game, I have created a Modlet that will add the castle to to the game POI when generating new worlds. See [here for instruction on how to install and use the Modlet](https://gitlab.com/spartan-designs/castle-spartan/blob/master/Mods/Modlet%20readme.md).

### Install 7 Days to Die

The POI can only be used with 7 Days to Die installed on a computer (sorry, no support for console). The latest version of the POI has been tested using Alpha 18.4 (b4) on a Windows 10 PC, but it should work with all Alpha 18 releases on PC, Mac, or Linux.

See _[How to install almost every past Steam version of 7 Days to Die](https://7daystodie.com/forums/showthread.php?106460-Guide-How-To-download-almost-every-past-Steam-version-of-7-Days-To-Die-from-Steam)_ for details instruction on downloading a specific version.

### Download the POI and copy the files

All the releases of the Castle can be [available here](https://gitlab.com/spartan-designs/castle-spartan/-/releases). Choose the latest :-).

You will then need to extract all the files from the archive **Mods\CasTaleSpartan\Prefabs** directory (hint: the file names all begin _CastleSpartan_) and copy those files in your local 7 Days to Die Prefabs directory. The path for the Prefabs folder is `<Steam library folder>\7 Days To Die\Data\Prefabs`. 

On a PC, the default location is `C:\Steam\steamapps\common\7 Days To Die\Data\Prefabs`. If you can't find the 7 Days to Die install folder, you can start your Steam client, navigate to the _Library-Games_, right-click on _7 Days to Die_, select _Properties_, and check the _LOCAL FILES_ tab.

#### _Caution_

There is not much bad that can happen by copying files in your game folder. That said, there is nothing I can do if you lose your game data, corrupt your game files, blow up your computer, or start a Zombie Apocalypse as a result of copying those files. You might want to backup to your game data and any prefab files that you've edited before messing around in the 7 Days to Die folder.

### Launch the game Prefab editor and load the POI

For this step, you start the game as usual, click the **EDITING TOOLS** menu, then click **LEVEL/PREFAB EDITOR**. 

Once the editor is loaded, press _ESC_ (or what whatever key brings you normally use to display the menu). There will be a window to the right of your screen. You need to click the **PREFAB BROWSER** (that's the fourth icon at the top that looks like little houses). 

On the second text box, type **CASTLESPARTAN** and press **LOAD**.

Do not worry if you spawn inside a wall. Castle Spartan is quite big so there is a good chance it will happen. Right now, you are in God mode. You can fly and you can walk through walls using the normal move key. (You need to dismiss the menu in order to move.)

### Start the Playtest

Even though you can already move around in the editor, the experience is not quite complete. It's more fun if you start the Playtest mode.

To do so, open the menu again, click the **DEBUG TOOLS** icon (the first one to the top left, it's a little hand with a wrench and cogwheels). You should now see a button labelled **PLAYTEST**. Press it and let the real visit begins.

There are no quests, zombies, or loot in Castle Spartan. There is no need for you to talk to the trader or to pick up weapons. (Maybe you will feel safer with a shotgun on your tool belt, it's up to you :-) ).

Grand Spartan did a very nice job with the lighting so the Castle looks better at night. You might want press _F1_ to bring up the console and type in the `settime night` command.

I hope you guys enjoy the visit as much as I did. Remember to visit [Grand Spartant YouTube channel](https://www.youtube.com/user/MrMMAFAN76) and show your appreciation.

### Using Castle Spartan in your game

It's easy, just follow the [instructions for the modlet installation](https://gitlab.com/spartan-designs/castle-spartan/blob/master/Mods/Modlet%20readme.md) and generate a new world.

## Changelog
### v3.1
- Neuter the candles, torches, and burning barrels so that the Castle doesn't attract a Bloom Moon 
  size horde of screamers every hour
  
### v3.0
- Make the POI (now called CastleSpartan_v3) work for Alpha 18
- No longer need to copy the POI file in the Prefab folder for the modlet to work

### 2.1
- The POI has loot and zombies, let the testing and tuning begin!

### 2.0
- First version of the POI files and modlet intended for actual gameplay
- Alterations were made to the original castle to make it level and to use generic terrain filler

### v1.2
- Added a mesh file for the long-distance rendering of the POI

### v1.1
- Added a thumbnail for the POI

### v1.0 
- First version of Castle Spartan POI
